import numpy as np
arr1=np.array([10,20,30,40])
arr2=arr1.copy()
print(arr1)
print(arr2)
print(id(arr1))
print(id(arr2))
print(id(arr1[0])==id(arr2[0]))
print("id of arr1: ",id(arr1[0]))
print("id of arr2: ",id(arr2[0]))
arr1[1]=60
print(arr1)
print(arr2)
print(id(arr1[1])==id(arr2[1]))
print(id(arr1)==id(arr2))
print(id(arr1))
print(id(arr2))
print("id of arr1 after changing: ",id(arr1[1]))
print("id of arr2 after changing: ",id(arr2[1]))


