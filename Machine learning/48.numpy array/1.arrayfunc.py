import numpy as np
arr1=np.array([10,20,30,40,50])
print(arr1)
print()

arr2=np.array([10,20,30,40.5,50])
print(arr2)
print()

arr3=np.array([10,20,30,40],int)
print(arr3)
print()

arr4=np.array([10,20,30,40.5,50],int)#priority for int(dtype) is high
print(arr4)
print()

arr5=np.array([10,20,30,40],str)
print(arr5)
print(arr5.itemsize)
print(arr5.dtype)
