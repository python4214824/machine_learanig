import pandas as pd
df=pd.read_csv("forestfires.csv")
print(df)
print()

print(df.shape)
print()

print(df.columns)
print()

print(df.size)
print()

print(df.head())
print()

print(df.tail(5))
print()

print(df[["month","day"]])
print()


print(df['temp'].max())
print()

print(df['temp'].min())
print()

print(df.describe())
print()

df.value_counts()

print(df['month'].value_counts)
print()

print(df[df['temp']>21])
print()

