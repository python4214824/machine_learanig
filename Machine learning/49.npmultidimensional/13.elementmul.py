import numpy as np

# Define two matrices of the same size
A = np.array([[1, 2, 3],
              [4, 5, 6]])

B = np.array([[7, 8, 9],
              [10, 11, 12]])

# Perform element-wise multiplication
product = np.multiply(A, B)

print("Element-wise Product:\n", product)

