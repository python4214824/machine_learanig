import numpy as np
arr1=np.matrix("1 2 3;4 5 6; 7 8 9")
print(arr1)
sumarr=arr1.sum()
print("sum: ",sumarr)

meanarr=arr1.mean()
print("mean: ",meanarr)

product=np.prod(arr1)
print("product: ",product)

product=arr1.prod(0)#col
print("product col wise: ",product)

product=arr1.prod(1)#row
print("product row wise: ",product)
