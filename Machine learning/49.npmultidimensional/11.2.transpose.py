import numpy as np

arr1=np.zeros((3,3),int)
print("array 1: ",arr1)
arr2=np.arange(1,10).reshape((3,3))
print("array 2: ",arr2)

for i in range(len(arr1)):
    for j in range(len(arr1[i])):
        arr1[i][j]=arr2[j][i]
print("transpose of array 2: ",(arr1))
