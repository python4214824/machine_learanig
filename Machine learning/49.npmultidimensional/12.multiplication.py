import numpy as np
arr1=np.matrix("1 2 3;3 4 5;6 7 8")
arr2=np.matrix("1 2 3;3 4 5;6 7 8")
sumarr=np.add(arr1,arr2)
print("addition using add func: ",sumarr)
print("arr1+arr2: ",arr1+arr2)
#same for sub.div
arr3=np.arange(1,10).reshape((3,3))
arr4=np.arange(1,10).reshape((3,3))
print(arr4)
print("product: ",(arr3*arr4))

print()
A = np.array([[1, 2, 3],
              [4, 5, 6]])

B = np.array([[7, 8],
              [9, 10],
              [11, 12]])
#print(A*B)
product = np.dot(A, B)
print("A AND B ",product)

